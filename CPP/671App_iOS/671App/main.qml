import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import Cryogen.Backend 1.0
import QtQuick.Dialogs 1.1


Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button {
            width: 120;
            height: 35;
            x: (Screen.desktopAvailableWidth - width) / 2;
            y: 100;
            text: "Send POST";
    //		onClicked: model.submit();
            onClicked: {
                messageDialog1.visible = true;
                back_end.send_POST();
                messageDialog2.visible = true;

    //			stack_view.push("qrc:/output_view.qml");
//                popup.open();
            }
        }

    MessageDialog {
        id: messageDialog1
        title: "Doing POST op 3000."
        text: ""
        onAccepted: {
            console.log("And of course you could only agree.")
        }
//        Component.onCompleted: visible = true
    }
    MessageDialog {
        id: messageDialog2
        title: "POST Done."
        text: ""
        onAccepted: {
            console.log("And of course you could only agree.")
        }
//        Component.onCompleted: visible = true
    }


}

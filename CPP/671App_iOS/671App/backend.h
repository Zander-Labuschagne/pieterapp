#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>

class Backend : public QObject
{
	Q_OBJECT
public:
	Backend();

public slots:
	void send_POST();
};

#endif // BACKEND_H

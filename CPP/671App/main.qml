import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import Pieter.Backend 1.0

Window {
	visible: true
	width: 640
	height: 480
	title: qsTr("Hello World")

	Button {
		width: 120;
		height: 35;
		text: "Send POST";
//		onClicked: model.submit();
		onClicked: {
			back_end.send_POST();
//			stack_view.push("qrc:/output_view.qml");
		}
	}
}

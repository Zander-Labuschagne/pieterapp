#ifndef BACKEND_HPP
#define BACKEND_HPP

#include <QObject>

class Backend : public QObject
{
	Q_OBJECT
public:
	Backend();


public slots:
	void send_POST();
};

#endif // BACKEND_HPP

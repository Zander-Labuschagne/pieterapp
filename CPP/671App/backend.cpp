#include <QtNetwork>
#include <QtJson>

#include "backend.hpp"

Backend::Backend()
{

}

void Backend::send_POST()
{
	//Doen POST
//	QNetworkRequest request(QUrl("http:192.168.1.195/water"));
//	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

//	QJsonObject json;
//	json.insert("naam", "Pieter");
//	json.insert("boodskap", "Pieter is nie lui as hy C++ app maak nie");

//	QNetworkAccessManager nam;
//	QNetworkReply *reply = nam.post(request, QJsonDocument(json).toJson());

//	// Wait for server response
//	while (!reply->isFinished()) {
//		qApp->processEvents();
//	}

//	// Store server response
//	QByteArray response_data = reply->readAll();
//	QJsonDocument json_doc = QJsonDocument::fromJson(response_data);
//	reply->deleteLater();

	QNetworkAccessManager *m_qnam;

	connect(m_qnam, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleNetworkData(QNetworkReply*)));
	connect(m_qnam,SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), this, SLOT(handleSSLErrors(QNetworkReply*)));

	// Build your JSON string as usual
	QByteArray jsonString = "{\"method\":\"AuthenticatePlain\",\"loginName\":\"username@domain.com\",\"password\":\"mypass\"}";

	// For your "Content-Length" header
	QByteArray postDataSize = QByteArray::number(jsonString.size());

	// Time for building your request
	QUrl serviceURL("https://www.superService.com/api/1.7/ssapi.asmx");
	QNetworkRequest request(serviceURL);

	// Add the headers specifying their names and their values with the following method : void QNetworkRequest::setRawHeader(const QByteArray & headerName, const QByteArray & headerValue);
	request.setRawHeader("User-Agent", "My app name v0.1");
	request.setRawHeader("X-Custom-User-Agent", "My app name v0.1");
	request.setRawHeader("Content-Type", "application/json");
	request.setRawHeader("Content-Length", postDataSize);

	// Use QNetworkReply * QNetworkAccessManager::post(const QNetworkRequest & request, const QByteArray & data); to send your request. Qt will rearrange everything correctly.
	QNetworkReply * reply = m_qnam->post(request, jsonString);


}
